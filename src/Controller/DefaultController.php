<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function home()
    {
        return $this->render('Default/index.html.twig');
    }

    /**
     * @Route("/private", name="private")
     */
    public function prive()
    {
        return $this->render('private/private.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
