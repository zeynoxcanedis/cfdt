<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titlePreview;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="formations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $formationStart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $formationEnd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitlePreview(): ?string
    {
        return $this->titlePreview;
    }

    public function setTitlePreview(string $titlePreview): self
    {
        $this->titlePreview = $titlePreview;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getFormationStart(): ?\DateTimeInterface
    {
        return $this->formationStart;
    }

    public function setFormationStart(\DateTimeInterface $formationStart): self
    {
        $this->formationStart = $formationStart;

        return $this;
    }

    public function getFormationEnd(): ?\DateTimeInterface
    {
        return $this->formationEnd;
    }

    public function setFormationEnd(\DateTimeInterface $formationEnd): self
    {
        $this->formationEnd = $formationEnd;

        return $this;
    }
}
